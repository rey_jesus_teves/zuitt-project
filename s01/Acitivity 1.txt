Application: Learning Management System
Features:
1. Replay Video Lessons
2. View and Grade Activities
3. View and Grade Exams
4. Manage Students
5. Manage Teachers

Aspects
1. Replay Video Lessons
-check if the video lesson can be viewed
-check if the video can be replayed
-check if video lesson can be saved in Linux
-check if video lesson can be save in Windows 10
-check if video lesson can be played at different speeds

2. View and Grade Activities
-check if student can view only his own activities
-check if teacher can view the activities of the student
-check if the grade created by the teacher was saved correctly
-check if the auto grading system is working correctly
-check if the average of the grades are computed correctly 

3. View and Grade Exams
-check if student can view only his own exams and correspond with his course
-check if teacher is able to grade the exams
-check if the correct answers are counted correctly
-check if the teacher can add or edit the exam
-check if the schedule of the exam can be viewed by the student

4. Manage Students
-check if new students can be added
-check if existing students can be check in the database
-check if the students can change password'
-check if students can change username
-check if students can view only his own information

5. Manage Teachers
-check if the teachers are being assigned to the correct subjects
-check if the teachers are being assigned to the corrrect students
-check if the teachers can login in the app using their username and password
-check if the teachers can open the app in their mobile devices
-check if the teachers can open the app both in Linux and Windows 10
